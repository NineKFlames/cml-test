# By-IP-finder
Ever got tired from those trolls on the internet? Well, here's your salvation!

### Find your enemies, just with one cURL!
To fire up the server you'll need only [`docker`](https://docs.docker.com/install/) and [`maven`](https://maven.apache.org/download.cgi) installed:
```
mvn clean install
docker-compose up --build geoip-app
```
### BOOM
Now they'll get back all the pain they caused to you!  
To track down your opponent, open your favourite browser and type in:
```
http://localhost:8080/geoip/{opponent-ip}
```
or, alternatively, use cURL if you're a console dweller:
```
curl http://localhost:8080/geoip/{opponent-ip}
```

Also, this project has a functional test [here](https://gitlab.com/NineKFlames/cml-functest).  
Data is used from [here](https://lite.ip2location.com/) – DB5.LITE
