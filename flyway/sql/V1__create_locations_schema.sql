create sequence geoip.geo_data_ipv4_id_seq
;

create table if not exists geo_data_ipv4
(
	start_range bigint not null,
	end_range bigint not null,
	country_code varchar(2) not null,
	country_name varchar(50) not null,
	country_region varchar not null,
	city varchar not null,
	latitude double precision not null,
	longitude double precision not null,
	id bigserial not null
		constraint geo_data_ipv4_id_pk
			primary key
)
;

create unique index if not exists geo_data_ipv4_id_uindex
	on geo_data_ipv4 (id)
;

create index if not exists geo_data_ipv4_start_range_end_range_index
	on geo_data_ipv4 (start_range, end_range)
;