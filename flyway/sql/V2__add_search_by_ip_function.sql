create or replace function find_location_by_integer_ip(bigint)
  returns setof geoip.geo_data_ipv4 as $$
    select *
    from geoip.geo_data_ipv4
    where geoip.geo_data_ipv4.start_range <= $1
          and $1 <= geoip.geo_data_ipv4.end_range;
$$
language sql;