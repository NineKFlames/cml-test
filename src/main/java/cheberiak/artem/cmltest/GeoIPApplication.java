package cheberiak.artem.cmltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("cheberiak.artem.cmltest")
@EnableJpaRepositories(basePackages = {"cheberiak.artem.cmltest.repository"})
public class GeoIPApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeoIPApplication.class);
    }
}
