package cheberiak.artem.cmltest.controllers;

import cheberiak.artem.cmltest.exceptions.IpNotFoundException;
import cheberiak.artem.cmltest.exceptions.MalformedIpException;
import cheberiak.artem.cmltest.model.GeologicalDataVO;
import cheberiak.artem.cmltest.services.Ip2GeoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;

@Controller
@RequestMapping("geoip")
public class GeoIpAPI {
    private static final Log LOGGER = LogFactory.getLog(GeoIpAPI.class);

    @Autowired
    private Ip2GeoService ip2GeoService;

    public GeoIpAPI(Ip2GeoService ip2GeoService) {
        this.ip2GeoService = ip2GeoService;
    }

    @GetMapping(value = "{ip}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GeologicalDataVO getGeolocationDataForIP(@PathVariable String ip)
            throws IpNotFoundException, UnknownHostException, MalformedIpException {
        LOGGER.info("Got IP request: " + ip);
        return ip2GeoService.getGeologicalDataFromCanonicalIp(ip);
    }

    @ExceptionHandler({IpNotFoundException.class, MalformedIpException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String exceptionHandler(Exception exception) {
        LOGGER.error(exception.getMessage(), exception);
        return exception.getMessage();
    }
}
