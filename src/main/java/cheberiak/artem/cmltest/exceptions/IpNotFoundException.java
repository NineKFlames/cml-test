package cheberiak.artem.cmltest.exceptions;

public class IpNotFoundException extends Exception {
    public IpNotFoundException() {
    }

    public IpNotFoundException(String message) {
        super(message);
    }

    public IpNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public IpNotFoundException(Throwable cause) {
        super(cause);
    }
}
