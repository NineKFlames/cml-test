package cheberiak.artem.cmltest.exceptions;

public class MalformedIpException extends Exception {
    public MalformedIpException() {
    }

    public MalformedIpException(String message) {
        super(message);
    }

    public MalformedIpException(String message, Throwable cause) {
        super(message, cause);
    }

    public MalformedIpException(Throwable cause) {
        super(cause);
    }
}
