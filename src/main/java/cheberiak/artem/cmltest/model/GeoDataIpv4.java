package cheberiak.artem.cmltest.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(schema = "geoip")
public class GeoDataIpv4 implements Serializable {
    @Id
    private Long id;

    @NotBlank
    private Long startRange;

    @NotBlank
    private Long endRange;

    @NotBlank
    private String countryCode;

    @NotBlank
    private String countryName;

    @NotBlank
    private String countryRegion;

    @NotBlank
    private String city;

    @NotBlank
    private Double latitude;

    @NotBlank
    private Double longitude;

    public Long getId() {
        return id;
    }

    public Long getStartRange() {
        return startRange;
    }

    public Long getEndRange() {
        return endRange;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCountryRegion() {
        return countryRegion;
    }

    public String getCity() {
        return city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
