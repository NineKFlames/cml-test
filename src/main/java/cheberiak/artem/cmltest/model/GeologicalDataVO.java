package cheberiak.artem.cmltest.model;

import java.io.Serializable;
import java.util.Objects;

public class GeologicalDataVO implements Serializable {
    private final String canonicalIPv4Representation;
    private final String cityName;
    private final String countryCode;
    private final String countryName;
    private final String regionName;
    private final long IPv4;
    private final double latitude;
    private final double longitude;

    public GeologicalDataVO(String canonicalIPv4Representation,
                            String cityName,
                            String countryCode,
                            String countryName, String regionName, long IPv4, double latitude, double longitude) {
        this.canonicalIPv4Representation = canonicalIPv4Representation;
        this.cityName = cityName;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regionName = regionName;
        this.IPv4 = IPv4;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "GeologicalDataVO{" +
               "canonicalIPv4Representation='" + canonicalIPv4Representation + '\'' +
               ", cityName='" + cityName + '\'' +
               ", countryCode='" + countryCode + '\'' +
               ", countryName='" + countryName + '\'' +
               ", regionName='" + regionName + '\'' +
               ", IPv4=" + IPv4 +
               ", latitude=" + latitude +
               ", longitude=" + longitude +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeologicalDataVO that = (GeologicalDataVO) o;
        return IPv4 == that.IPv4 &&
               Double.compare(that.latitude, latitude) == 0 &&
               Double.compare(that.longitude, longitude) == 0 &&
               Objects.equals(canonicalIPv4Representation, that.canonicalIPv4Representation) &&
               Objects.equals(cityName, that.cityName) &&
               Objects.equals(countryCode, that.countryCode) &&
               Objects.equals(countryName, that.countryName) &&
               Objects.equals(regionName, that.regionName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(canonicalIPv4Representation,
                            cityName,
                            countryCode,
                            countryName,
                            regionName,
                            IPv4,
                            latitude,
                            longitude);
    }

    public String getCanonicalIPv4Representation() {
        return canonicalIPv4Representation;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getRegionName() {
        return regionName;
    }

    public long getIPv4() {
        return IPv4;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
