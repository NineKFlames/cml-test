package cheberiak.artem.cmltest.repository;

import cheberiak.artem.cmltest.model.GeoDataIpv4;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * {@link JpaRepository} for retrieving location information based on integer-form IPv4.
 */
@Repository
public interface Ip2GeoRepository extends JpaRepository<GeoDataIpv4, Long> {
    /**
     * Uses a custom function top retrieve location data based on integer-form IPv4.
     * @param ipAsInteger integer-form IPv4.
     * @return {@link GeoDataIpv4} instance with location data for provided integer-form IPv4.
     */
    @Transactional
    @Query(value = "SELECT * FROM geoip.find_location_by_integer_ip(:ipAsInteger)",
           nativeQuery = true)
    Optional<GeoDataIpv4> getLocationFromIp(@Param("ipAsInteger") long ipAsInteger);
}
