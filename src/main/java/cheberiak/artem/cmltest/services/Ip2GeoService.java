package cheberiak.artem.cmltest.services;

import cheberiak.artem.cmltest.exceptions.IpNotFoundException;
import cheberiak.artem.cmltest.exceptions.MalformedIpException;
import cheberiak.artem.cmltest.model.GeoDataIpv4;
import cheberiak.artem.cmltest.model.GeologicalDataVO;
import cheberiak.artem.cmltest.repository.Ip2GeoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * A {@link Service} that provides access to {@link Ip2GeoRepository}.
 */
@Service
public class Ip2GeoService {
    /**
     * Regex for evaluating canonical IPv4 addresses.
     */
    private static final String IP_REGEX = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

    /**
     * {@link Ip2GeoRepository} that provides information about IPv4 address plug-in location.
     */
    @Autowired
    private Ip2GeoRepository repository;

    public Ip2GeoService(Ip2GeoRepository repository) {
        this.repository = repository;
    }

    /**
     * Evaluates the IPv4, gets location for it and returns it as {@link GeoDataIpv4}.
     * @param canonicalIpAsString {@link String} that contains a canonical IPv4 address, like 8.8.8.8.
     * @return geolocation data.
     * @throws IpNotFoundException if failed to retrieve location info for given IPv4.
     * @throws UnknownHostException by {@link Ip2GeoService#canonicalIpStringToInt(String)}.
     * @throws MalformedIpException if {@code canonicalIpAsString} provided is not a canonical IPv4 address.
     */
    public GeologicalDataVO getGeologicalDataFromCanonicalIp(String canonicalIpAsString)
            throws IpNotFoundException, UnknownHostException, MalformedIpException {
        checkIpSanity(canonicalIpAsString);
        long ipAsInt = canonicalIpStringToInt(canonicalIpAsString);
        Optional<GeoDataIpv4> optionalLocationFromIp = repository.getLocationFromIp(ipAsInt);
        GeoDataIpv4 locationFromIp = optionalLocationFromIp.orElseThrow(() -> new IpNotFoundException(
                "You have requested for ip: " + canonicalIpAsString +
                ", but we counldn't find it in our database! Could you please check it?"));
        return new GeologicalDataVO(canonicalIpAsString,
                                    locationFromIp.getCity(),
                                    locationFromIp.getCountryCode(),
                                    locationFromIp.getCountryName(),
                                    locationFromIp.getCountryRegion(),
                                    ipAsInt,
                                    locationFromIp.getLatitude(),
                                    locationFromIp.getLongitude());
    }

    /**
     * Converts a {@link String} with canonical IPv4 address into an {@code int} representation.
     * @param canonicalIpAsString {@link String} that contains a canonical IPv4 address, like 8.8.8.8.
     * @return IPv4 address as an {@code int}.
     * @throws UnknownHostException thrown by {@link InetAddress} if couldn't parse the address.
     */
    private int canonicalIpStringToInt(String canonicalIpAsString) throws UnknownHostException {
        int ipAsInt = 0;

        for (byte octet : InetAddress.getByName(canonicalIpAsString).getAddress()) {
            ipAsInt = ipAsInt << 8 | octet;
        }

        return ipAsInt;
    }

    /**
     * Checks if the {@link String} provided is a canonical IPv4 representation.
     * @param ip {@link String} to be checked.
     * @throws MalformedIpException if the {@link String} provided is not a canonical IPv4 representation.
     */
    private void checkIpSanity(String ip) throws MalformedIpException {
        if (!ip.matches(IP_REGEX)) {
            throw new MalformedIpException("Dude, is that in IP? I don't think so.");
        }
    }
}
